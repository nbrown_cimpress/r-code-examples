library(shiny)
library(plotly)
library(shinydashboard)
library(elo)
source("app_helper.R")

ui <- dashboardPage(
  dashboardHeader(title = "UFC Dashboard"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem("Weight Class",
               tabName = "weight_class_tab",
               icon = icon("dashboard")),
      menuItem("Head to Head",
               tabName = "head_tab",
               icon = icon("dashboard")),
      menuItem("Fighter",
               tabName = "fighter_tab",
               icon = icon("dashboard"))
    )
  ),
  
  dashboardBody(
    tabItems(
      tabItem(tabName = "weight_class_tab",
              box(plotlyOutput("elo_timeseries")),
              box(plotlyOutput("elo_dist")),
              box(DT::dataTableOutput("top_5_table")),
              box(uiOutput("v_weight_class")),
              box(sliderInput(inputId = "v_k_1",
                              label = "K for ELO",
                              min = 1,
                              max = 100,
                              value = 20))),
      tabItem(tabName = "head_tab",
              fluidRow(box(uiOutput("fighter_selector")), box(uiOutput("opponent_selector"))),
              fluidRow(box(valueBoxOutput("fighter_card")), box(valueBoxOutput("opponent_card"))),
              fluidRow(box(uiOutput("v_weight_class_2"), sliderInput(inputId = "v_k_2",
                                                                          label = "K for ELO",
                                                                          min = 1,
                                                                          max = 100,
                                                                          value = 20)))
              ),
      tabItem(tabName = "fighter_tab",
              fluidRow(box(uiOutput("fighter_selector_2")), box(uiOutput("v_weight_class_3"))),
              fluidRow(box(DT::dataTableOutput("fighter_desc")), box(plotlyOutput("fighter_radar")))
              )
    )
  )
)

server <- function(input, output) {
  
  df <- reactive({
    req(input$v_k_1)
    create_elo_data(elo_df, input$v_k_1)
  })
  
  df_2 <- reactive({
    req(input$v_k_2)
    create_elo_data(elo_df, input$v_k_2)
  })
  
  top_5 <- reactive({
    df() %>%
      filter(weight_class == input$v_weight_class) %>% 
      group_by(fighter) %>%
      arrange(desc(elo)) %>%
      slice(1) %>%
      ungroup() %>%
      top_n(elo, n = 5) 
  })
  
  output$v_weight_class <- ui('v_weight_class', ufc_df)
  output$v_weight_class_2 <-  ui('v_weight_class_2', ufc_df)
  output$v_weight_class_3 <-  ui('v_weight_class_3', ufc_df)
  
  output$fighter_selector <-  renderUI({
    selectInput(inputId = 'v_fighter',
                label = "Fighter",
                choices = df_2() %>% 
                  filter(weight_class == input$v_weight_class_2) %>% 
                  select(fighter) %>% 
                  distinct() %>% 
                  arrange(fighter))
  })
  output$fighter_selector_2 <-   renderUI({
    selectInput(inputId = 'v_fighter_2',
                label = "Fighter",
                choices = ufc_df %>% 
                  filter(weight_class == input$v_weight_class_3) %>% 
                  select(fighter) %>% 
                  distinct() %>% 
                  arrange(fighter))
  })
  
  output$opponent_selector <-  renderUI({
    selectInput(inputId = "v_opponent",
                label = "Opponent",
                choices = df_2() %>% 
                  filter(fighter != input$v_fighter &
                          weight_class == input$v_weight_class_2) %>% 
                  select(fighter) %>% 
                  distinct() %>% 
                  arrange(fighter))
  })

  
  
  output$top_5_table <- DT::renderDataTable({
      DT::datatable(top_5() %>%
      arrange(desc(elo)) %>%
      mutate(rank = row_number()) %>% 
      select(fighter, elo, rank)) })
   
  output$elo_timeseries <- renderPlotly({
       t5 <- top_5() %>% 
         select(fighter)
       df_5 <- df() %>% 
         filter(fighter %in% t5$fighter)
         
      df_plot <- df() %>% 
        filter(weight_class == input$v_weight_class) 
        ggplotly(
          ggplot(data = df_plot, aes(date,elo)) +
            geom_point() +
            geom_point(data = df_5, aes(date,elo, color = fighter)) +
            theme_light() +
            labs(
               x = NULL,
               y = "ELO Rating"
            )) %>%
          layout(legend = list(orientation = "h",   # show entries horizontally
                               xanchor = "center",  # use center of legend as anchor
                               x = 0.5, y =-0.2))            
      
   })
    
  output$elo_dist <- renderPlotly({
     df_hist <- df() %>% 
      filter(weight_class == input$v_weight_class) 
      ggplotly(
        ggplot(data = df_hist, aes(elo)) + 
          geom_histogram(fill = 'lightblue', color = 'black', bins = 20) +
          theme_light()
      )
     
  })
  
  output$fighter_card <- renderValueBox({
    elo <- elo.run(winner ~ fighter + opponent,
                   k = input$v_k_2,
                   data = elo_df)
    fighter_prob <- round(100 * predict(elo, data.frame(fighter = input$v_fighter, opponent = input$v_opponent)),0)
    valueBox(
      value = paste0(fighter_prob, "%"),
      subtitle = paste0(input$v_fighter, " Probability"),
      color = 'blue',
      icon = icon("hand-rock")
    )
  })
  
  output$opponent_card <- renderValueBox({
    elo <- elo.run(winner ~ fighter + opponent,
                   k = input$v_k_2,
                   data = elo_df)
    opponent_prob <- round(100 * predict(elo, tibble(fighter = input$v_opponent, opponent = input$v_fighter)),0)
    valueBox(
      value = paste0(opponent_prob, "%"),
      subtitle = paste0(input$v_opponent, " Probability"),
      color = 'red',
      icon = icon("hand-rock")
    )
  })
  
  output$fighter_desc <- DT::renderDataTable({
    a <- ufc_df %>%
      filter(fighter == input$v_fighter_2) %>%
      group_by(weight_class ) %>% 
      summarise(fights = n())
    b <- ufc_df %>%
      filter(fighter == input$v_fighter_2) %>%
      select(weight_class, 'height cm' = Height_cms, 'reach cm' = Reach_cms, stance = Stance, age, wins, losses)
    
    c <- b %>% left_join(a, by = 'weight_class')
    
    
    
    DT::datatable(c %>%
       filter(weight_class == input$v_weight_class_3),
      options = list(paging = FALSE,
                     searching = FALSE),
      rownames= FALSE)
})
  
  output$fighter_radar <- renderPlotly({
    r_df <- radar_df %>% 
      filter(Fighter == input$v_fighter_2)
    plot_ly(
      type = "scatterpolar",
      mode = 'markers',
      r = r_df$value,
      theta = r_df$name,
      fill = "toself"
    ) %>% 
      layout(title = paste0(input$v_fighter_2))
  })
}

shinyApp(ui, server)